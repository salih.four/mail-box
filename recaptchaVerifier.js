 const { recaptchaSecret } = require('./config');
 const fetch = require('node-fetch');

 module.exports = async function(recaptchaToken) {
    return new Promise(function(resolve, reject) {
        let url = 'https://www.google.com/recaptcha/api/siteverify'
            +'?secret='
            +recaptchaSecret
            +'&response='
            +recaptchaToken;
        fetch(url, {
            method: "POST"
        })
            .then((verifyRes) => {
                console.log(verifyRes);
                if(verifyRes.status !== 200) throw "Connectivity issue at server";
                return verifyRes.json();
            })
            .then((verifyRes) => {
                console.log(verifyRes);
                if(verifyRes.success) resolve(true);
                else throw "Could not verify recaptcha";
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            });
    });
 };