const mailModel = require('./mail-model');

module.exports = {
    clearMails: async function(){
        try {
            await mailModel.deleteMany({}).exec();
            return true;
        } catch (err) {
            return false;
        }
    },
    dumpMails: async function(){
        return mailModel.find({}, { from: 1, date: 1, content: 1 }).exec();
    }
}