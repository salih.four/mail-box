const secretary = require('./secretary');
const { mailBoxPassKey } = require('./config');
const recaptchaVerifier = require('./recaptchaVerifier');

module.exports = {
    getSecretaryByPassKey: async function(passKey, recaptchaToken) {
        return new Promise(function(resolve, reject) {
            recaptchaVerifier(recaptchaToken)
                .then((_) => {
                    if(passKey !== mailBoxPassKey) {
                        reject("Wrong pass key");
                    }
                    resolve(secretary);
                })
                .catch((err) => {
                    reject(err);
                })          
        })
    }
}