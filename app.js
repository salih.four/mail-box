const express = require('express');
const app = express();
const secretary = require('./secretary');
const { postOfficeCtrl, mailBoxCtrl } = require('./controllers');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.json({msg:"Hello"});
});

app.use('/public', express.static('./public'));

app.post('/api/post-office', postOfficeCtrl);

app.get('/api/mail-box', mailBoxCtrl);

module.exports = app;