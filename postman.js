const mailModel = require('./mail-model');

module.exports = {
    sendMail: async function(content, from){
        return new Promise(function(resolve, reject) {
            let mail = new mailModel({
                content, from
            });
            mail.save()
                .then((_) => {
                    resolve(true);
                })
                .catch((err) => {
                    reject(err);
                });
        })
    }
}