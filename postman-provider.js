const postman = require('./postman');
const recaptchaVerifier = require('./recaptchaVerifier');

module.exports = {
    getPostmanByReCaptcha: async function(captchaToken){
        return new Promise(function(resolve, reject) {
            recaptchaVerifier(captchaToken)
                .then((_) => {
                    resolve(postman);
                })
                .catch((err) => reject(err));
        });
    }
}