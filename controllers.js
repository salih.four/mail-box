const postmanProvider = require('./postman-provider');
const secretaryProvider = require('./secretary-provider');

module.exports = {
    postOfficeCtrl: (req, res) => {
        if(req.body.permit === "recaptcha"){
            postmanProvider.getPostmanByReCaptcha(req.body["g-recaptcha-response"])
                .then((postman) => {
                    let mail = req.body;
                    postman.sendMail(mail, "Guest")
                        .then((bool) => {
                            res.json({ message: "Mail send" });
                        })
                        .catch((err) => {
                            res.status(500).json({err});
                        })
                })
                .catch((err) => {
                    if(err === "recaptcha") res.status(400).json({ message: "Are you a robot?"});
                    res.status(500).json({err});
                });
        } else {
            res.status(401).json({message: "Permission Denied"})
        }
    },
    mailBoxCtrl: (req, res) => {
        let permit = req.query.permit;
        if(permit === "passkey"){
            let passKey = req.query["passkey"];
            let recaptchaToken = req.query["g-recaptcha-response"];
            secretaryProvider.getSecretaryByPassKey(passKey, recaptchaToken)
                .then((secretary) => {
                    secretary.dumpMails()
                        .then((dump) => {
                            res.json(dump);
                        })
                        .catch((err) => {
                            res.status(500).json({err});
                        })
                })
                .catch((err) => {
                    res.status(400).json({err});
                })
        } else {
            res.status(401).json({});
        }
    }
}